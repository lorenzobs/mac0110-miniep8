function compareByValue(x,y)

	valorx = x[1:length(x)-1]
	
	if valorx == "J"
		valorx = "11"		
	elseif valorx == "Q"
		valorx = "12"
	elseif valorx == "K"
		valorx = "13"
	elseif valorx == "A"
		valorx = "14"
	end
	a = parse(Int64,valorx)

	valory = y[1:length(y)-1]
	if valory == "J"
		valory = "11"		
	elseif valory == "Q"
		valory = "12"
	elseif valory == "K"
		valory = "13"
	elseif valory == "A"
		valory = "14"
	end
	b = parse(Int64,valory)

	if a < b
		return true
	else
		return false
	end
end

function compareByValueAndSuit(x,y)

	if x[length(x)] == '♦'
		naipe1 = 1
	elseif x[length(x)] == '♠'
		naipe1 = 2
	elseif x[length(x)] == '♥'
		naipe1 = 3
	else
		naipe1 = 4
	end
	
	if y[length(y)] == '♦'
		naipe2 = 1
	elseif y[length(y)] == '♠'
		naipe2 = 2
	elseif y[length(y)] == '♥'
		naipe2 = 3
	else
		naipe2 = 4
	end

	if naipe1 < naipe2
		return true
	elseif naipe1 > naipe2
		return false
	end

	return compareByValue(x,y)
end

function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j],v[j-1])
				troca(v, j, j - 1)
			else
				break
			end
			j = j - 1
		end
	end
	return v
end

using Test
function testaValor()
	@test (compareByValue("2♠", "A♠"))
	@test (!compareByValue("K♥", "10♥"))
	@test (!compareByValue("10♠", "10♥"))
	@test (!compareByValue("A♠","A♠"))
	println("Fim dos testes")
end
testaValor()

function testaNaipe()
	@test (compareByValueAndSuit("2♠", "A♠"))
	@test (!compareByValueAndSuit("K♥", "10♥"))
	@test (compareByValueAndSuit("10♠", "10♥"))
	println("Fim dos testes")
end
testaNaipe()

function testaInsertion()
	@test (insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦","J♠","K♠","A♠","A♠","10♥"])
	@test (insercao([]) == [])
	@test (insercao(["4♥","5♥","7♥","3♥","Q♥","J♥","A♥"]) == ["3♥","4♥","5♥","7♥","J♥","Q♥","A♥"])
	println("Fim dos testes")
end
testaInsertion()